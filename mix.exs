defmodule Evenbot.MixProject do
  use Mix.Project

  def project do
    [
      app: :evenbot,
      version: "0.1.0",
      elixir: "~> 1.13",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      # applications: [:logger_file_backend, :logger],
      extra_applications: [:logger],
      mod: {Evenbot.Application, []}
    ]
  end

  # defp applications(_all), do: [:logger] ++ [:logger_file_backend]
  # defp applications(:dev), do: applications(:all)

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      # {:ex_gram, git: "https://github.com/rockneurotiko/ex_gram.git"},
      {:ex_gram, "~> 0.26"},
      {:tesla, "~> 1.4"},
      {:hackney, "~> 1.12"},
      {:jason, ">= 1.3.0"},
      {:logger_file_backend, "~> 0.0.13"},
      {:pathex, "~> 2.0"}
      # {:remix, "~> 0.0.1", only: :dev}
    ]
  end
end
