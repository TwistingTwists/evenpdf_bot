defmodule Evenbot.QuestionsServer do
  @moduledoc """
  simple storage server to store the time from /config  command

  Usage :
  In telegram bot :
  /config 9

  the method there calls,update(text)
  """
  import Pathex
  use GenServer
  import Evenbot.Helpers

  @poll_interval :timer.seconds(10)

  def start_link(q_list, chat_id) do
    GenServer.start_link(__MODULE__, q_list, name: server_name(chat_id))
  end

  def next_q(chat_id) do
    "next_q functoin call" |> green
    GenServer.call(server_name(chat_id), :next)
  end

  @impl true
  def init(q_list) do
    # {count, interval} = Keyword.fetch!(opts, :interval
    # {:ok, schedule_next(q_list, @poll_interval)}
    q_list |> purple()
    {:ok, q_list}
  end

  @impl true
  def handle_call(:next, _from, [] = state) do
    # send(self(), :quit)
    {:reply, :finished, state}
  end

  @impl true
  def handle_call(:next, _from, [q | rest] = state) do
    state |> green
    {:reply, q, rest}
  end

  # @impl true
  # def handle_info(:quit, state) do
  #   "in quit" |> purple
  #   {:stop, :normal, state}
  # end

  # defp schedule_next([q | rest], after_ms \\ @poll_interval) do
  #   Process.send_after(self(), :next, after_ms)
  #   rest
  # end

  def server_name(chat_id) do
    "Q#{chat_id}" |> String.to_atom()
  end
end
