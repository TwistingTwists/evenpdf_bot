defmodule Evenbot do
  @moduledoc """
  Documentation for `Evenbot`.
  """

  @doc """
  Hello world.

  ## Examples

      iex> Evenbot.hello()
      :world

  """
  def hello do
    :world
  end
end
