defmodule Evenbot.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application
  require Logger

  @impl true
  def start(_type, _args) do
    Logger.debug("application.ex - started with #{inspect(self())}")
    token = "5333670741:AAEPYVnOjpypYtbVvgJuNsDyac2eOdo9y8c"
    token_margin_bot ="token"
    # token_sfg = "1937885367:AAECwizH8e_65YtJixV3xz7xQwHZ36zaDMQ"

    # heroku_456 bot - for test purpose
    # token_sfg = "5206840820:AAHYvoCOGJiU2Zpfs4F4UFdOI5n-vAsGXnc"
    token_rato = "5298534931:AAGFINEL3Y15W3F-85uBdmwkggVjBxgFuSU"

    children = [
      # Starts a worker by calling: Evenbot.Worker.start_link(arg)
      # {Evenbot.Worker, arg}
      ExGram,
      {Evenbot.Bot, [method: :polling, token: token]},
      # {Evenbot.MarginBot, [method: :polling, token: token_margin_bot]},
      {Evenbot.RatoBot, [method: :polling, token: token_rato]}
      # Evenbot.ConfigAgent
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Evenbot.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
