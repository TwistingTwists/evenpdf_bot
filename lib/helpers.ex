defmodule Evenbot.Helpers do
  @moduledoc """
  for helpers functoins to be used in bots
  """

  def sanitise_filename(file_name) when is_binary(file_name) do
    special_character_list = ["!", "@", "#", "$", "%", "^'", "&", "*", "(", ")"]

    file_name
    |> String.replace(special_character_list, " ")
    |> String.replace(" ", "_")
    |> green()
  end

  @doc """
    separate even pages in pdf file
    > using command line - qpdf
  """
  def run_even(file_name) do
    run_doc(file_name, "even")
  end

  def run_odd(file_name) do
    run_doc(file_name, "odd")
  end

  defp run_doc(file_name, odd_even) do
    file_name |> IO.inspect()
    odd_even = String.trim(odd_even)

    head_file = String.split(file_name, ".pdf") |> hd
    even_file = head_file <> "_" <> odd_even <> ".pdf"

    cmd_string =
      (" qpdf " <> file_name <> " --pages . 1-z:" <> odd_even <> " -- " <> even_file)
      |> blue()

    :os.cmd(cmd_string |> to_charlist) |> green()
    even_file
  end

  @doc """
  downloads the file by file_id (given by telegram api) and saves it to disk in @outdir
  """
  def download_file(file_id, file_name, outdir, bot) do
    with {:ok, file} <- ExGram.get_file(file_id, bot: bot) do
      url = ExGram.File.file_url(file, bot: bot)
      {:ok, response} = Tesla.get(url)
      out_file = outdir <> file_name
      File.write!(out_file, response.body)
    else
      _ -> nil
    end
  end

  # color io functions
  @the_types [
    :number,
    :atom,
    :string,
    :boolean,
    :binary,
    :bitstring,
    :list,
    :map,
    :regex,
    :tuple,
    :function,
    :struct,
    :pid,
    :port,
    :reference,
    :date,
    :datetime,
    nil
  ]
  @green Enum.map(@the_types, fn t -> {t, :green} end)
  @red Enum.map(@the_types, fn t -> {t, :red} end)
  @yellow Enum.map(@the_types, fn t -> {t, :yellow} end)
  @blue Enum.map(@the_types, fn t -> {t, :cyan} end)
  @purple Enum.map(@the_types, fn t -> {t, IO.ANSI.color(4, 2, 5)} end)
  @orange Enum.map(@the_types, fn t -> {t, IO.ANSI.color(4, 2, 0)} end)

  @multi Enum.map(@the_types, fn t ->
           case t do
             :number -> {t, :yellow}
             :pid -> {t, :red}
             :bitstring -> {t, :cyan}
             :map -> {t, :green}
             :atom -> {t, IO.ANSI.color(4, 2, 0)}
             :date -> {t, :blue}
             :datetime -> {t, :blue}
             :list -> {t, IO.ANSI.color(1, 5, 0)}
             # :string -> {t, IO.ANSI.color(4,2,5)}
             # :binary -> {t, IO.ANSI.color(0,2,5)}
             _ -> {t, IO.ANSI.color(4, 2, 5)}
           end
         end)

  def green(data) do
    IO.inspect(data, syntax_colors: @green)
  end

  def red(data) do
    IO.inspect(data, syntax_colors: @red)
  end

  def yellow(data) do
    IO.inspect(data, syntax_colors: @yellow)
  end

  def blue(data) do
    IO.inspect(data, syntax_colors: @blue)
  end

  def orange(data) do
    IO.inspect(data, syntax_colors: @orange)
  end

  def purple(data) do
    IO.inspect(data, syntax_colors: @purple)
  end

  def log3(data) do
    purple(data)
  end

  def log2(data) do
    orange(data)
  end

  def log(data) do
    IO.inspect(data, syntax_colors: @multi)
  end

  def get_json(filename) do
    with {:ok, body} <- File.read(filename), {:ok, json} <- Jason.decode(body), do: json
  end
end
