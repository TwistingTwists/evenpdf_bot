defmodule Evenbot.SFGBot do
  @bot :sfg_l3_bot
  # @next_online "3"

  import Evenbot.Helpers
  alias Evenbot.ConfigAgent
  require Logger

  # @outdir "pdfs/"

  use ExGram.Bot,
    name: @bot

  @spec bot :: :sfg_l3_bot
  def bot(), do: @bot

  def handle({:command, "start", _msg}, context) do
    _msg |> blue
    answer(context, "Hi!")
  end

  def handle({:text, "sa", msg}, context) do
    pid = ConfigAgent.start()

    Logger.debug("Config Agent has pid #{inspect(pid)}")

    context
    |> answer("configagent started - #{inspect(pid)}.")
  end

  def handle({:text, text, msg}, context) do
    time = get_config()
    msg |> IO.inspect()

    Logger.info(date: msg.date)

    answer(
      context,
      "SFG Bot is offine now. Next online time  = #{time}"
    )
  end

  def handle({:command, "config", msg}, context) do
    msg |> yellow
    %{text: text, from: %{first_name: first_name, id: 1_018_169_911}} = msg
    text |> blue
    # first_name |> green

    set_config(text)

    context |> answer("next online time is set as #{get_config()}. ")
  end

  def handle({:inline_query, msg}, context) do
    msg |> green
    answer_inline_query(context, "articles")
  end

  # def get_config(), do: @next_online
  def get_config(), do: ConfigAgent.get()
  def set_config(text), do: ConfigAgent.update(text)
end
