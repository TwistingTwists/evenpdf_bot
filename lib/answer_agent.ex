defmodule Evenbot.AnswerAgent do
  @moduledoc """
  simple storage server to store the time from /config  command

  Usage :
  In telegram bot :
  /config 9

  the method there calls,update(text)
  """
  import Pathex
  import Evenbot.Helpers

  def start(chat_id, qs) do
    # Creating
    state = generate_struct_with_msgid(qs)
    Agent.start_link(fn -> state end, name: agent_name(chat_id))
  end

  def update(chat_id, q_text, msg_id) do
    # Update
    Agent.update(agent_name(chat_id), fn state -> state |> update_state(q_text, msg_id) end)
  end

  def get(q_text, chat_id) do
    # Retreive
    # todo refractor to use actual message id
    q_text |> green

    Agent.get(agent_name(chat_id), fn state -> state |> match_q_text(q_text, 01) end)
  end

  def agent_name(chat_id) do
    "A#{chat_id}" |> String.to_atom()
  end

  def update_state(state, "", _), do: state
  def update_state(state, q_text, msg_id), do: match_q_text(state, q_text, msg_id)

  # def match_q_text([q_text = q["ques"] | rest], q_text, msg_id), do: q["msg_id"] = msg_id

  def match_q_text(
        [%{"ques" => qs, "ans" => ans, "msg_id" => msgid} = q | rest],
        q_text,
        msg_id
      ) do
    if String.trim(qs) == q_text do
      # q_text |> yellow
      # path_to_msg = Pathex.path("msg_id")
      ans |> orange
    else
      match_q_text(rest, q_text, msg_id)
    end

    # [Pathex.set!(q, path_to_msg, msg_id) | rest]
  end

  def match_q_text([q | rest], q_text, msg_id) do
    q["ques"] |> purple
    [q | match_q_text(rest, q_text, msg_id)]
  end

  def generate_struct_with_msgid([]), do: %{}
  def generate_struct_with_msgid([q]), do: Map.put(q, "msg_id", 0)

  def generate_struct_with_msgid([q | rest]) do
    [
      Map.put(q, "msg_id", 0) | generate_struct_with_msgid(rest)
    ]
  end
end

# defmodule Testing do
#   def generate_struct_with_msgid([]), do: %{}
#   def generate_struct_with_msgid([q]), do: Map.put(q, "msg_id", 0)

#   def generate_struct_with_msgid([q | rest]) do
#     [Map.put(q, "msg_id", 0) | generate_struct_with_msgid(rest)]
#   end

#   def match_q_text(
#         [%{"ques" => q_text, "ans" => ans, "msg_id" => msgid} = q | rest],
#         q_text,
#         msg_id
#       ) do
#     path_to_msg = Pathex.path("msg_id")
#     [Pathex.set!(q, path_to_msg, msg_id) | rest]
#   end

#   def match_q_text([q | rest], q_text, msg_id),
#     do: [q | match_q_text(rest, q_text, msg_id)]
# end
