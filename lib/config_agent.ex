defmodule Evenbot.ConfigAgent do
  @moduledoc """
  simple storage server to store the time from /config  command

  Usage :
  In telegram bot :
  /config 9

  the method there calls,update(text)
  """
  @spec start :: {:error, any} | {:ok, pid}
  def start() do
    # Creating
    Agent.start_link(fn -> "3" end, name: Storage)
  end

  def update(time) do
    # Update
    Agent.update(Storage, fn state -> state <> " _ " <> time end)
  end

  def get() do
    # Retreive
    Agent.get(Storage, fn state -> state end)
  end
end
