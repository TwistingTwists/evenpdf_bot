defmodule Evenbot.Bot do
  @bot :evenpdf_bot

  @outdir "pdfs/"

  use ExGram.Bot,
    name: @bot

  import Evenbot.Helpers
  require Logger
  def bot(), do: @bot
  def outdir(), do: @outdir

  def handle({:command, "start", _msg}, context) do
    answer(context, "Hello!")
  end

  def handle({:text, "cc" <> files, _msg}, context) do
    curl_commands =
      files
      # |> yellow()
      |> String.split("\n")
      |> Enum.map(fn x -> String.trim(x) end)
      |> Enum.filter(fn x -> x != "" end)
      # |> yellow()
      |> convert_filename_to_curl

    context
    |> answer("\n\n" <> curl_commands)
  end

  def handle({:text, _text, _msg}, context) do
    answer(
      context,
      "This bot is to be meant for use internally by sfg_bot. Nothing you should need to know! :) focus on your preparation!"
    )
  end

  def handle({:inline_query, msg}, context) do
    msg |> IO.inspect()
    answer_inline_query(context, "articles")
  end

  def handle(
        {:update, %{channel_post: %{chat: %{id: chat_id}, document: document}} = msg},
        context
      ) do
    # ensure two things
    # 1. bot is added as admin in the channel where you post document
    # 2. bot is added as admin in the channel where you WANT TO POST message

    %{file_name: filename} = document

    ExGram.send_message(
      sfg_channel(),
      "#{filename} - \n uploaded for paid subscribers. \n @sfg_back_bot ",
      bot: bot()
    )
  end

  def handle(
        {:update,
         %{channel_post: %{chat: %{id: chat_id}, text: "@evenpdf_bot" <> text_msg}} = msg},
        _context
      ) do
    msg |> yellow

    chat_id |> green
    text_msg |> yellow

    %{channel_post: %{reply_to_message: reply_message_with_document}} = msg

    %{document: %{file_id: file_id, file_name: file_name}} = reply_message_with_document

    file_name = file_name |> sanitise_filename()
    download_file(file_id, file_name, outdir(), bot()) |> IO.inspect()

    file = outdir() <> file_name
    even_file = run_even(file)
    odd_file = run_odd(file)

    case String.trim(text_msg) do
      "both" ->
        ("SENDING BOTH CHAT " <> even_file) |> IO.inspect()
        ExGram.send_document(chat_id, {:file, even_file}, bot: bot())
        ExGram.send_document(sfg_channel(), {:file, odd_file}, bot: bot())

      "odd" ->
        ("SENDING ONLY HINDI       " <> even_file) |> IO.inspect()
        # also send hindi medium SFG to channel
        ExGram.send_document(sfg_channel(), {:file, odd_file}, bot: bot())

      _ ->
        ("\n\nSENDING only ENGLISH to SFGpaid channel\n\n " <> even_file) |> IO.inspect()
        ExGram.send_document(chat_id, {:file, even_file}, bot: bot())
    end
  end

  def handle(
        {:update, msg},
        _context
      ) do
    %{channel_post: %{chat: %{title: chat_title}, text: msg_text}} = msg
    ExGram.send_message(kabira(), "#{msg_text} is on #{chat_title} ", bot: bot())
  end

  def handle({:message, msg}, context) do
    chat_id = msg.chat.id
    %{document: %{file_id: file_id, file_name: file_name}} = msg

    # %{forward_from_chat: %{id: original_id, title: original_chat_name, type: original_chat_type}} =
    #   msg

    # remove empty spaces in filename
    file_name = file_name |> sanitise_filename() |> red()
    [even_file, odd_file] = download_even_odd(file_id, file_name)

    Logger.debug(
      evenfile: even_file,
      chat_id: chat_id
      # original_chat_id: original_id,
      # chat_title: original_chat_name,
      # chat_type: original_chat_type
    )

    ExGram.send_document(chat_id, {:file, even_file}, bot: bot())

    Logger.debug(
      oddfile: odd_file,
      chat_id: chat_id
      # original_chat_id: original_id,
      # chat_title: original_chat_name,
      # chat_type: original_chat_type
    )

    ExGram.send_document(chat_id, {:file, odd_file}, bot: bot())
  end

  def download_even_odd(file_id, file_name) do
    download_file(file_id, file_name, outdir(), bot()) |> IO.inspect()
    file = (outdir() <> file_name) |> orange()
    even_file = run_even(file) |> yellow()
    odd_file = run_odd(file) |> yellow()
    [even_file, odd_file]
  end

  # def handle({:update, msg}, context) do
  #   %{channel_post: %{chat: %{id: chat_id}}} = msg
  #   chat_id |> IO.inspect()

  #   %{channel_post: %{reply_to_message: reply_message_with_document}} = msg

  #   %{document: %{file_id: file_id, file_name: file_name}} = reply_message_with_document

  #   download_file(file_id, file_name, outdir(), bot()) |> IO.inspect()

  #   file = outdir() <> file_name
  #   even_file = run_even(file)

  #   ("\n\nSENDING DOCUMENT TO THE CHAT\n\n " <> even_file) |> IO.inspect()
  #   ExGram.send_document(chat_id, {:file, even_file}, bot: bot())
  # end

  # newsfg bot group id
  defp sfg_channel, do: -1_001_642_484_983

  @doc """
  takes a list of filenames and converts the filename to curl command for uploading on anonfiles

  """
  def convert_filename_to_curl(filenames) when is_binary(filenames) do
    do_convert([filenames])
  end

  def convert_filename_to_curl(filenames) when is_list(filenames) do
    filenames |> green
    do_convert(filenames)
  end

  def do_convert([hd]),
    do:
      (" curl --progress-bar -F 'file=@" <>
         hd <>
         "'  'https://api.anonfiles.com/upload?token=6de07998a95f020d' >> ./anonfiles_vision.txt  ")
      |> blue

  def do_convert([hd | tail]), do: do_convert(tail) <> "\n\n" <> do_convert([hd])

  defp kabira, do: 5_319_999_856
end
