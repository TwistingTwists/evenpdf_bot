defmodule Evenbot.RatoBot do
  @bot :rato_bot
  # @next_online "3"

  import Evenbot.Helpers
  alias Evenbot.ConfigAgent
  alias Evenbot.QuestionsServer
  alias Evenbot.AnswerAgent
  require Logger

  # @outdir "pdfs/"

  use ExGram.Bot,
    name: @bot

  def bot(), do: @bot

  def handle({:command, "start", msg}, context) do
    %{"IBPS_clerk 2022" => ques} = get_json("./lib/tempdb/ibpsclerk.json")

    # ques |> yellow
    telegram_id = msg.chat.id

    QuestionsServer.start_link(ques, telegram_id) |> yellow
    AnswerAgent.start(telegram_id, ques) |> purple
    first = QuestionsServer.next_q(telegram_id) |> red

    if first != :finished do
      answer(context, "#{first["ques"]}")
    else
      answer(context, "quiz finished!")
    end

    # do_send_questions(telegram_id, ques)
  end

  def handle({:text, text, msg}, context) do
    msg_id = msg.reply_to_message.message_id
    q_text = msg.reply_to_message.text |> green

    chat_id = msg.chat.id |> yellow
    ans = AnswerAgent.get(q_text, chat_id) |> yellow
    ExGram.send_message(chat_id, "#{ans}", bot: bot())

    next_q = QuestionsServer.next_q(chat_id) |> red

    unless next_q == :finished do
      ExGram.send_message(chat_id, "#{next_q["ques"]}", bot: bot())
    end

    # answer(context, "#{ans}")
  end

  # def do_send_questions(_telegram_id, []), do: :quiz_finished

  def do_send_questions(telegram_id, [q_resp | q_resps]) do
    send_question(telegram_id, q_resp)

    receive do
      :next ->
        # send_question(telegram_id, instance_id, puid, q_resp)
        do_send_questions(telegram_id, q_resps)

      something_else ->
        log("received odd message")
        log(something_else)
    end
  end

  defp send_question(telegram_id, q_resp) do
    case ExGram.send_message(telegram_id, q_resp["ques"], bot: bot()) do
      {:ok,
       %ExGram.Model.Message{
         message_id: telegram_sent_msg_id,
         text: question_text
       }} ->
        # TODO think about a race condition in which the user response
        # arrives before we have saved the msg_id
        # log("trying to record_sent_question")

        ("-- -- -- -- -- -- \n-- telegram sent message id is -- " <>
           inspect(telegram_sent_msg_id))
        |> purple

      error ->
        log("some error happened.")
        log(error)
    end
  end
end
