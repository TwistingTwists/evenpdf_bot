import Config

config :logger,
  compile_time_purge_level: :debug,
  backends: [
    :console,
    {LoggerFileBackend, :info},
    {LoggerFileBackend, :debug},
    {LoggerFileBackend, :error}
    # {LoggerFileBackend, :log_event}
    # {LoggerFileBackend, :backends_log}
  ],
  format: "[$level] $message\n"

config :logger, :info,
  path: "log/info.log",
  level: :info,
  metadata: :all

config :logger, :error,
  path: "log/error.log",
  level: :error,
  metadata: :all

config :logger, :debug,
  path: "log/debug.log",
  level: :debug,
  metadata: :all

# config :logger, :backends_log,
#   path: "log/awesome.log",
#   level: :debug,
#   metadata: :all

# config :logger, :log_event,
#   path: '/Users/abhishektripathi/Downloads/march2022/evenpdf_bot/logevent.log',
#   level: :log_event
