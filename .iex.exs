# alias Evenbot.QuestionsServer, as: QS
# # alias Evenbot.ConfigAgent, as: CAgent
# # alias Evenbot.SFGBot, as: Sfgbot
# import Evenbot.Helpers
# import Pathex

# %{"IBPS_clerk 2022" => ques} = get_json("./lib/tempdb/ibpsclerk.json")

# defmodule Testing do
#   def generate_struct_with_msgid([]), do: %{}
#   def generate_struct_with_msgid([q]), do: Map.put(q, "msg_id", 0)

#   def generate_struct_with_msgid([q | rest]) do
#     [Map.put(q, "msg_id", 0) | generate_struct_with_msgid(rest)]
#   end

#   def match_q_text(
#         [%{"ques" => q_text, "ans" => ans, "msg_id" => msgid} = q | rest],
#         q_text,
#         msg_id
#       ) do
#     path_to_msg = Pathex.path("msg_id")
#     [Pathex.set!(q, path_to_msg, msg_id) | rest]
#   end

#   def match_q_text([q | rest], q_text, msg_id),
#     do: [q | match_q_text(rest, q_text, msg_id)]
# end

# ques_struct = Testing.generate_struct_with_msgid(ques)

# # q_text = "3.The Indian Navy commissioned INS Vela, the 4th _______ Submarine, at Mumbai. "

# # Testing.match_q_text(ques_struct,q_text,186)

# # chat_id = 1

# # {:ok, pid} = QS.start_link(ques,2)
